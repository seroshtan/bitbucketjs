BABEL_NODE = ./node_modules/.bin/babel-node
BROWSERIFY = ./node_modules/.bin/browserify
UGLIFY = ./node_modules/.bin/uglifyjs
EXORCIST = ./node_modules/.bin/exorcist
BABEL = ./node_modules/.bin/babel
TAPE = ./node_modules/.bin/tape
MOCHA = ./node_modules/.bin/mocha
SRC = $(wildcard *.c)
LIB = $(subst src,lib,$(SRC))

build: lib dist/bitbucket.js

test:
	$(MOCHA) --compilers js:mocha-babel test/**/*.js

watch:
	fswatch -o $(shell pwd) | xargs -n1 -I{} make

lib:
	$(BABEL) src --source-maps -d lib

dist/bitbucket.debug.js:
	$(BROWSERIFY) -t babelify . --full-paths -s bitbucketjs -d > $@

dist/bitbucket.js:
	$(BROWSERIFY) -t babelify . -s bitbucketjs -d | exorcist dist/bitbucket.js.map > $@

dist/bitbucket.min.js: dist/bitbucket.js dist/bitbucket.js.map
	$(UGLIFY) $< --in-source-map dist/bitbucket.js.map --screw-ie8 -o $@ --source-map $(addsuffix .map, $@) --source-map-include-sources --source-map-url bitbucket.js.map

clean:
	rm -rf lib

.PHONY: test lib dist/bitbucket.js
