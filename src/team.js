import urlJoin from 'url-join' ;
import debug from 'debug';
import { authenticated } from './decorators';

const log = debug('bitbucketjs:repo');
const path = '/2.0/teams';

module.exports = function (opts) {
  const request = opts.request;

  return {
    fetch(teamname) {
      return request
        .get(urlJoin(opts.apiRoot, path, teamname))
    },

    followers(teamname) {
      return request
        .get(urlJoin(opts.apiRoot, path, teamname, 'followers'))
    },

    @authenticated(request)
    mine(role='member') {
      return request
        .get(urlJoin(opts.apiRoot, path))
        .query({'role': role})
    }
  }
}
