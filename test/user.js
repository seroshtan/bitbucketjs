import { expect } from './chai';
import { getClient, config } from './util';

import sinon from 'sinon';
import join from 'url-join';
import nockback from './nockback';

describe('fetching a user given their username', () => {
  it('returns the matching user', () => {
    let bitbucket = getClient();
    sinon.spy(bitbucket.request, 'get');

    return nockback('user/wbinnssmith.json',
      bitbucket.user.fetch('wbinnssmith')
        .then((user) => {
          expect(user.username).to.equal('wbinnssmith');
          expect(user.type).to.equal('user');
          expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'users', 'wbinnssmith'));
        })
      );
  })
})

describe('fetching a user\'s followers given their username', () => {
  it('returns the user\'s followers', () => {
    let bitbucket = getClient();
    sinon.spy(bitbucket.request, 'get');

    return nockback('user/wbinnssmith.followers.json',
      bitbucket.user.followers('wbinnssmith')
        .then((followers) => {
          expect(followers).to.have.property('pagelen');
          expect(followers).to.have.property('page');
          expect(followers.values).to.be.instanceof(Array);
          expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'users', 'wbinnssmith', 'followers'));
        })
      );
  })
})
